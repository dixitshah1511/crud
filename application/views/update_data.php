<?php 


foreach ($result as  $value) {
  $emp_id=$value->emp_id;
  $emp_fname=$value->emp_fname;
  $emp_lname=$value->emp_lname;

}


 ?>
  <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update Data</h4>
      </div>
      <div class="modal-body">
        	<form id="from_update" method="post">
         	  <div class="err_update" style='color: #2c6923;font-weight: 700;'></div> 		
				<input type="hidden" value="<?php echo $emp_id; ?>" name="emp_id">
			  <div class="form-group">
			    <label for="email">First Name</label>
			    <input type="email" class="form-control" name="emp_fname" autocomplete="off" value="<?php echo $emp_fname; ?>" >
			  </div>
			  <div class="form-group">
			    <label for="pwd">Last Name</label>
			    <input type="text" class="form-control" name="emp_lname" autocomplete="off" value="<?php echo $emp_lname; ?>" >
			  </div>
			  
			  <button type="button" class="btn btn-default" id="btn_update">Update</button>
			</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
