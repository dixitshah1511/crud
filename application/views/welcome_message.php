

<div class="container">
	
	<div class="row">
		<h3 class="text-center"> CRUD SYSTEM</h3>
		<div class="col-sm-4">
			<br><br>
			
			<form id="from_add" method="post">
         	  <div class="err" style='color: #2c6923;font-weight: 700;'></div> 		
				
			  <div class="form-group">
			    <label for="email">First Name</label>
			    <input type="email" class="form-control" name="emp_fname" autocomplete="off" >
			  </div>
			  <div class="form-group">
			    <label for="pwd">Last Name</label>
			    <input type="text" class="form-control" name="emp_lname" autocomplete="off" >
			  </div>
			  
			  <button type="button" class="btn btn-default" id="btn_form">Add</button>
			</form>
		</div>		
		<div class="col-sm-8">
				
				<br><br>
				
				<table class="table table-striped">
				    <thead>
				      <tr>
				        <th>Firstname</th>
				        <th>Lastname</th>
				        <th>Action</th>
				      </tr>
				    </thead>
				    <tbody>
				     	<?php 
				     		if(is_array($result)){
				     	 	foreach ($result as  $value) {
				     	 		
				     	



				     	 ?>

				      <tr>
				        <td><?php echo $value->emp_fname; ?></td>
				        <td><?php echo $value->emp_lname; ?></td>
				        <td>
				        		<a  href="<?php echo base_url(); ?>Welcome/select_data/<?php  echo $value->emp_id; ?>" data-toggle="modal" data-target="#myModal" class="btn btn-default btn-sm" >Edit</a>
				        		<a href="<?php echo base_url(); ?>Welcome/delete/<?php echo $value->emp_id; ?>" class="btn btn-danger btn-sm">Delete</a>

				        </td>
				      </tr>
				  <?php }} ?>
				    
				    </tbody>
				  </table>




		</div>
	</div>
</div>

 <div id="myModal" class="modal fade" role="dialog">
   <div class="modal-dialog">

  
   <?php 

  		$this->load->view('update_data');

   ?>
	</div>
</div>

