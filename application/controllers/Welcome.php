<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {


	public function index()
	{
		$ans=$this->myclass->select_data("emp_id,emp_fname,emp_lname","emp","1");
		$this->load->view("header");
		$this->load->view("welcome_message",array("result"=>$ans));
		$this->load->view("footer");

	}
	public function delete($id){
		$this->myclass->delete_data("emp","emp_id='$id'");
		redirect("Welcome/index");
	}

	public function add(){

		$ans=$this->input->post();


		$this->form_validation->set_rules('emp_fname','First Name','trim|required');
		$this->form_validation->set_rules('emp_lname', 'Last Name', 'trim|required');
		
		if($this->form_validation->run()==FALSE)
				{
					echo "<div style='color: #e61212;font-weight: 700;'> ".validation_errors()."</div>";
				}	
				else
				{
					// print_r($ans);
					$this->myclass->insert("emp",$ans);
					echo 1;
				}
		
	}	

	public function select_data($id){
		

		$ans=$this->myclass->select_data("emp_id,emp_fname,emp_lname","emp","emp_id='$id'");
		$this->load->view("header");
		$this->load->view("update_data",array("result"=>$ans));
		$this->load->view("footer");




	}	
	
	public function update(){
		$ans=$this->input->post();


		$this->form_validation->set_rules('emp_fname','First Name','trim|required');
		$this->form_validation->set_rules('emp_lname', 'Last Name', 'trim|required');
		
		if($this->form_validation->run()==FALSE)
				{
					echo "<div style='color: #e61212;font-weight: 700;'> ".validation_errors()."</div>";
				}	
				else
				{
					
					$id=$ans['emp_id'];
					$option=array(
						
						'emp_fname'=>$this->input->post('emp_fname'),
						'emp_lname'=>$this->input->post('emp_lname')
						
						);
					$result=$this->myclass->update_data("emp",$option,"emp_id='$id'");
					echo 1;
				}
	}
	
}
